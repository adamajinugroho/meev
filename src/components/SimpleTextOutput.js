import React, { Fragment } from "react";
import styles from "./TextOutput.module.scss";

export default function SimpleTextOutput({ caption, value, link }) {
  return (
    <Fragment>
      <p className={`col-sm-12 col-md-2 ${styles.textlabel}`}>{caption}</p>
      <div className={`col-sm-12 col-md-10`}>
        {!link ? (
          <p className={styles.textcontent}>{value}</p>
        ) : (
          <a className={styles.urlcontent} href={value}>
            {value}
          </a>
        )}
      </div>
    </Fragment>
  );
}
