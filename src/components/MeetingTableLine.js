import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import * as MeetingStatusType from "../types/MeetingStatus";

export default function MeetingTableLine({
  id,
  event,
  date,
  time,
  pic,
  attachment,
  status,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id,
    event,
    date,
    time,
    pic,
    attachment,
    status,
  });
  const [isActive, setActive] = useState(false);

  useEffect(() => {
    setLine({ id, event, date, time, pic, attachment, status });
  }, []);

  // useEffect(() => {
  //   updateValue();
  // });

  function updateValue() {
    console.log(line);
    onChange(line);
  }

  function deleteLine() {
    onDelete(line.id);
  }

  const toggleMenu = (e) => {
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    if (isActive) {
      setActive(false);
    }
  };

  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    window.addEventListener("click", disableMenu);
    return () => window.removeEventListener("click", disableMenu);
  });

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-3">
          <span>{line.event}</span>
        </div>
        <div className="col-2">
          <span>{line.date}</span>
        </div>
        <div className="col-2">
          <span>{line.time}</span>
        </div>
        <div className="col-2">
          <span>{line.pic}</span>
        </div>
        <div className="col-2">
          {line.attachment ? (
            <div class="d-flex align-items-center justify-content-start pointer">
              <a
                href={line.evidence}
                className="d-flex align-items-center text-primary"
              >
                <i
                  className="fas fa-download"
                  style={{ fontSize: "1em", marginRight: 5 }}
                ></i>
                <span> Download</span>
              </a>
            </div>
          ) : (
            // <a href={line.attachment} className="text-primary">
            //   Download
            // </a>
            <span>None</span>
          )}
        </div>
        <div className="col-1">
          <span
            className={`badge ${
              line.status === MeetingStatusType.NYS
                ? "badge-primary"
                : line.status === MeetingStatusType.CANCEL
                ? "badge-danger"
                : "badge-success"
            }`}
            style={{ width: "80px" }}
          >
            {line.status}
          </span>
        </div>
      </div>

      <div class="dropdown row col-1 align-items-center justify-content-end pr-1">
        <button
          type="button"
          className="blank-button d-flex align-items-center justify-content-center"
          onClick={toggleMenu}
        >
          <i className="fas fa-ellipsis-v" style={{ fontSize: "1em" }}></i>
        </button>
        <div
          className={`dropdown-menu ${isActive ? "d-block" : "d-none"}`}
          style={{ top: 25, right: 10, left: "auto" }}
        >
          <Link
            class="dropdown-item mb-1 small-menu"
            to={"/meeting/edit/" + id}
          >
            Edit
          </Link>
          <Link
            class="dropdown-item mb-1 small-menu"
            to={"/meeting/report/" + id}
          >
            Report
          </Link>
          <Link
            class="dropdown-item mb-1 small-menu"
            to={"/meeting/detail/" + id}
          >
            Detail
          </Link>
          <button
            class="dropdown-item text-danger small-menu blank-button"
            onClick={deleteLine}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}
