import React from "react";
import styles from "./Identity.module.scss";
import photo from "../assets/photo1.png";

export default function Identity() {
  return (
    <div className="d-flex align-items-center mx-4">
      <div className="rounded-circle bg-dark" style={{ width: 40, height: 40 }}>
        <img style={{ width: 40, height: 40 }} src={photo} />
      </div>
      <div style={{ marginLeft: 20 }}>
        <p className={`font-weight-bold ${styles.text}`}>John Doe</p>
        <p className={styles.text}>234512351</p>
      </div>
    </div>
  );
}
