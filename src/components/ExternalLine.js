import React, { useState, useEffect } from "react";

export default function ExternalLine({
  id,
  name,
  information,
  contact,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id: "",
    name: "",
    information: "",
    contact: "",
  });

  useEffect(() => {
    setLine({ id, name, information, contact });
  }, []);

  function deleteLine() {
    onDelete(line);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4">
          <input
            className="form-control"
            id={`extname${id}`}
            type="text"
            name={`extname${id}`}
            onChange={(e) => {
              const newline = { ...line, name: e.target.value };
              setLine(newline);
              onChange(newline);
            }}
            value={name}
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>
        <div className="col-4">
          <input
            className="form-control"
            id={`extinformation${id}`}
            type="text"
            name={`extinformation${id}`}
            onChange={(e) => {
              const newline = { ...line, information: e.target.value };
              setLine(newline);
              onChange(newline);
            }}
            value={information}
            required
          />
        </div>
        <div className="col-4">
          <input
            className="form-control"
            id={`extcontact${id}`}
            type="text"
            name={`extcontact${id}`}
            onChange={(e) => {
              const newline = { ...line, contact: e.target.value };
              setLine(newline);
              onChange(newline);
            }}
            value={contact}
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
