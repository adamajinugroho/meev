import React, { Fragment, useState, useEffect, useRef } from "react";

import { withResizeDetector } from "react-resize-detector";
const AdaptiveWithDetector = ({ width, height, callback, children }) => {
  useEffect(() => {
    callback();
  }, [width]);

  return <div>{children}</div>;
};
export default withResizeDetector(AdaptiveWithDetector);
