import React, { Fragment, useState, useEffect } from "react";

export default function LocationTextInput({
  offlineloc,
  onlineloc,
  change,
  placeholder,
}) {
  const [offline, setOffline] = useState("");
  const [online, setOnline] = useState("");
  const [enableOnline, setEnableOnline] = useState(false);
  const [enableOffline, setEnableOffline] = useState(true);

  // useEffect(() => {
  //   setOffline(offlineloc);
  //   setOnline(onlineloc);
  // }, []);

  useEffect(() => {
    setOffline(offlineloc);
    setOnline(onlineloc);
  }, [offlineloc, onlineloc]);

  return (
    <Fragment>
      <label
        className="col-sm-12 col-md-2"
        style={{ marginTop: "0.0em" }}
        for="location"
      >
        Location
        <span className="text-danger ml-0">*</span>
      </label>
      <div className="col-sm-12 col-md-10">
        <div class="custom-control custom-checkbox custom-checkbox-red form-control-xs mb-2">
          <input
            type="checkbox"
            class="custom-control-input custom-control-input-red"
            id="offline"
            checked={enableOffline}
            onChange={() => {
              setEnableOffline(!enableOffline);
            }}
          />
          <label
            class="custom-control-label"
            style={{ color: "#212529" }}
            for="offline"
          >
            Offline
          </label>
        </div>
        <input
          className="form-control"
          id="locoffline"
          type="text"
          name="Locoffline"
          onChange={change}
          value={offline}
          placeholder={placeholder}
          disabled={!enableOffline}
          required={enableOffline}
        />
        {enableOffline && !offline ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}

        <div class="custom-control custom-checkbox custom-checkbox-red mt-4  mb-2 form-control-xs">
          <input
            type="checkbox"
            class="custom-control-input custom-control-input-red"
            id="online"
            checked={enableOnline}
            onChange={() => {
              setEnableOnline(!enableOnline);
            }}
          />
          <label
            class="custom-control-label"
            style={{ color: "#212529" }}
            for="online"
          >
            Online
          </label>
        </div>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">
              URL
            </span>
          </div>
          <input
            className="form-control"
            id="loconline"
            type="text"
            name="Loconline"
            onChange={change}
            value={online}
            placeholder={placeholder}
            disabled={!enableOnline}
            required={enableOnline}
          />
        </div>

        {enableOnline && !online ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
