import React, { Fragment, useState, useEffect } from "react";

import chroma from "chroma-js";
import Select from "react-select";

export default function ChipSelectInput({
  caption,
  name,
  value,
  availableNames,
  change,
  placeholder,
  required,
}) {
  // const [personOptions, setPersonOptions] = useState(
  //   availableNames?.personOptions
  // );

  // const [groupOptions, setGroupOptions] = useState(
  //   availableNames?.groupOptions
  // );

  // const [externalOptions, setExternalOptions] = useState(
  //   availableNames?.externalOptions
  // );

  const [combinedOptions, setCombinedOptions] = useState();

  const [thevalue, setTheValue] = useState();

  // useEffect(() => {
  //   setTheValue(value);
  // }, []);

  useEffect(() => {
    const curval = value?.map((o) => {
      return { value: o.id, label: o.name, color: o.color || "#4B4B4D" };
    });
    setTheValue(curval);
  }, [value]);

  useEffect(() => {
    const personOptions = availableNames?.personOptions;
    const groupOptions = availableNames?.groupOptions;
    const externalOptions = availableNames?.externalOptions;
    if (personOptions || groupOptions || externalOptions) {
      setCombinedOptions([
        {
          label: "Group Audience",
          options: groupOptions,
        },
        {
          label: "Internal Audience",
          options: personOptions,
        },
        {
          label: "External Audience",
          options: externalOptions,
        },
      ]);
    }
  }, [availableNames]);

  const colourStyles = {
    control: (styles) => ({
      ...styles,
      backgroundColor: "white",
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      const color = chroma(data.color);
      return {
        ...styles,

        fontSize: "0.9em",
        backgroundColor: isSelected
          ? data.color
          : isFocused
          ? color.alpha(0.1).css()
          : null,
        color: isSelected
          ? chroma.contrast(color, "white") > 2
            ? "white"
            : "black"
          : data.color,

        ":active": {
          ...styles[":active"],
          backgroundColor:
            !isDisabled && (isSelected ? data.color : color.alpha(0.3).css()),
        },
      };
    },
    multiValue: (styles, { data }) => {
      const color = chroma(data.color);
      return {
        ...styles,
        borderRadius: 40,
        overflow: "hidden",
        backgroundColor: color.alpha(0.1).css(),
      };
    },
    multiValueLabel: (styles, { data }) => ({
      ...styles,
      color: data.color,
    }),
    multiValueRemove: (styles, { data }) => ({
      ...styles,
      color: data.color,
      ":hover": {
        backgroundColor: data.color,
        color: "white",
      },
    }),
  };

  function updateValue(e) {
    console.log("e", e);
    const names = e?.map((o) => {
      return { id: o.value, name: o.label, color: o.color };
    });
    setTheValue(e);
    change(names);
  }

  return (
    <Fragment>
      <label
        className="col-sm-12 col-md-2"
        style={{ marginTop: "0.5em" }}
        for={name}
      >
        {caption}
        {required ? <span className="text-danger ml-0">*</span> : ""}
      </label>
      <div className="col-sm-12 col-md-10">
        <Select
          isMulti
          options={combinedOptions}
          styles={colourStyles}
          name="colors"
          placeholder={placeholder}
          className="basic-multi-select"
          classNamePrefix="select"
          onChange={updateValue}
          value={thevalue}
        />
        {required && (!thevalue || thevalue?.length === 0) ? (
          <div className="invalid-feedback custom-invalid">
            Field is required
          </div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
