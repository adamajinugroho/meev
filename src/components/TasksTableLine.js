import React, { Fragment, useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import * as TasksStatusType from "../types/TasksStatus";
import * as GeneralStatusType from "../types/GeneralStatus";
import Moment from "moment";

export default function TasksTableLine({
  id,
  name,
  description,
  evidence,
  status,
  due,
  approved,
  creator,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id,
    name,
    description,
    evidence,
    status,
    due: Moment(new Date(due)).format("DD-MM-YYYY"),
    approved,
    creator,
  });
  const [isActive, setActive] = useState(false);

  useEffect(() => {
    setLine({
      id,
      name,
      description,
      evidence,
      status,
      due: Moment(new Date(due)).format("DD-MM-YYYY"),
      approved,
      creator,
    });
  }, []);

  useEffect(() => {
    updateValue();
  });

  const uploadEl = useRef(null);

  function updateValue() {
    console.log(line);
    onChange(line);
  }

  function deleteLine() {
    onDelete(line.id);
  }

  function changeStatus() {
    toggleMenu();
    const newstatus =
      line.status === TasksStatusType.NY
        ? TasksStatusType.ON_PROGRESS
        : line.status === TasksStatusType.ON_PROGRESS
        ? TasksStatusType.DONE
        : TasksStatusType.ON_PROGRESS;

    setLine({ ...line, status: newstatus });
    // onChange(line)
  }

  function changeApproved() {
    toggleMenu();
    const newapp =
      line.approved === GeneralStatusType.NO
        ? GeneralStatusType.YES
        : GeneralStatusType.NO;

    setLine({ ...line, approved: newapp });
    // onChange(line)
  }

  function deleteEvidence() {
    toggleMenu();
    setLine({ ...line, evidence: "" });
    // onChange(line)
  }

  const toggleMenu = (e) => {
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    if (isActive) {
      setActive(false);
    }
  };

  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    window.addEventListener("click", disableMenu);
    return () => window.removeEventListener("click", disableMenu);
  });

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-2">
          <span>{line.name}</span>
        </div>
        <div className="col-3">
          <span>{line.description}</span>
        </div>
        <div className="col-2">
          <span>{line.due}</span>
        </div>
        <div className="col-2">
          {line.evidence ? (
            <div class="d-flex align-items-center justify-content-start">
              <a
                href={line.evidence}
                className="d-flex align-items-center text-primary"
              >
                <i
                  className="fas fa-download"
                  style={{ fontSize: "1em", marginRight: 5 }}
                ></i>
                <span> Download</span>
              </a>
            </div>
          ) : (
            <div class="d-flex align-items-center justify-content-start">
              <button
                type="button"
                className="blank-button"
                onClick={() => {
                  uploadEl.current.click();
                }}
              >
                <a className="d-flex align-items-center text-primary">
                  <i
                    className="fas fa-upload"
                    style={{ fontSize: "1em", marginRight: 5 }}
                  ></i>
                  <span> Upload</span>
                </a>
              </button>

              <input
                hidden
                type="file"
                ref={uploadEl}
                id="fileupload"
                name="file"
                // accept=".zip"
                onChange={(e) =>
                  setLine({
                    ...line,
                    evidence: "https://www.7-zip.org/a/7za920.zip",
                  })
                }
              />
            </div>
          )}
        </div>
        <div className="col-2">
          <span
            className={`badge ${
              line.status === TasksStatusType.ON_PROGRESS
                ? "badge-primary"
                : line.status === TasksStatusType.NY
                ? "badge-warning"
                : "badge-success"
            }`}
            style={{ width: "100px" }}
          >
            {line.status}
          </span>
        </div>
        <div className="col-1 d-flex justify-content-center">
          {line.approved === GeneralStatusType.YES ? (
            <span className="text-success" key="uniqueApproved">
              <i
                className="fas fa-check-circle"
                style={{ fontSize: "1em" }}
              ></i>
            </span>
          ) : (
            <div className="text-danger" key="uniqueUnApproved">
              <i
                className="fas fa-times-circle"
                style={{ fontSize: "1em" }}
              ></i>
            </div>
          )}
          {/* <span
            className={`badge ${
              line.approved === GeneralStatusType.NO
                ? "badge-warning"
                : "badge-success"
            }`}
            style={{ width: "50px" }}
          >
            {line.approved}
          </span> */}
        </div>
      </div>

      <div class="dropdown row col-1 align-items-start justify-content-end pr-1">
        <button
          type="button"
          className="blank-button d-flex align-items-center justify-content-center"
          onClick={toggleMenu}
        >
          <i className="fas fa-ellipsis-v" style={{ fontSize: "1em" }}></i>
        </button>
        <div
          className={`dropdown-menu ${isActive ? "d-block" : "d-none"}`}
          style={{ top: 25, right: 10, left: "auto" }}
        >
          <button
            className="dropdown-item mb-1 small-menu"
            onClick={changeStatus}
          >
            Set
            <span style={{ fontWeight: 500 }}>
              {` "${
                line.status === TasksStatusType.NY
                  ? TasksStatusType.ON_PROGRESS
                  : line.status === TasksStatusType.ON_PROGRESS
                  ? TasksStatusType.DONE
                  : TasksStatusType.ON_PROGRESS
              }"`}
            </span>
          </button>
          <button
            className={`dropdown-item mb-1 ${
              line.creator === true ? "d-block" : "d-none"
            } small-menu`}
            onClick={changeApproved}
          >
            Set
            <span style={{ fontWeight: 500 }}>
              {` "${
                line.approved === GeneralStatusType.NO
                  ? "Approved"
                  : "Not Approved"
              }"`}
            </span>
          </button>
          <button
            className={`dropdown-item mb-1 ${
              line.evidence ? "" : "d-none"
            } small-menu text-danger`}
            onClick={deleteEvidence}
          >
            Delete Evidence
          </button>
        </div>
      </div>
    </div>
  );
}
