import React, { useState, useEffect } from "react";
import NotificationLine from "./NotificationLine";
import * as ActivityStatusType from "../types/ActivityStatus";

export default function Notifications() {
  const [isActive, setActive] = useState(false);

  const toggleMenu = (e) => {
    e.stopPropagation();
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    if (isActive) {
      setActive(false);
    }
  };

  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    document.addEventListener("click", disableMenu);
    return () => document.removeEventListener("click", disableMenu);
  });

  return (
    <div class="dropdown ml-2">
      <button
        type="button"
        className="blank-button d-flex align-items-center"
        onClick={toggleMenu}
      >
        <div
          style={{
            position: "absolute",
            right: 5,
            bottom: 2,
            borderRadius: 5,
            border: "solid 1px white",
            width: 7,
            height: 7,
            backgroundColor: "red",
          }}
        ></div>
        <i className="far fa-bell" style={{ fontSize: "21px" }}></i>
      </button>
      <div
        className={`dropdown-menu ${
          isActive ? "d-block" : "d-none"
        } py-0 overflow-auto`}
        style={{ top: 30, right: 0, left: "auto" }}
      >
        <div
          className="row mx-0 px-1 py-2 border-bottom"
          style={{ width: 325, backgroundColor: "#ececec" }}
        >
          <span
            style={{ fontWeight: 600, fontSize: "0.9em" }}
            className="col-12"
          >
            Notifications
          </span>
        </div>
        <div style={{ maxHeight: 400 }}>
          <NotificationLine
            class="dropdown-item"
            type={ActivityStatusType.MEETING}
            content="Event 1"
            date="10-12-2020"
          />
          <NotificationLine
            class="dropdown-item"
            type={ActivityStatusType.CHAIRMAN}
            content="Event 2"
            date="10-12-2020"
          />
          <NotificationLine
            class="dropdown-item"
            type={ActivityStatusType.TASK}
            content="Task 1"
            date="10-12-2020"
          />
        </div>
      </div>
    </div>
  );
}
