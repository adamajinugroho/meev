import React, { Fragment } from "react";

export default function PICDetailsLine({ id, name, task, description }) {
  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-12">
        <div className="col-4">
          <span>{name}</span>
        </div>

        <div className="col-4">
          <span>{task}</span>
        </div>

        <div className="col-4">
          <span>{description}</span>
        </div>
      </div>
    </div>
  );
}
