import React, { Fragment } from "react";
import styles from "./SectionBar.module.scss";

export default function SectionBar({ title, subtitle }) {
  return (
    <div
      className={`d-flex flex-column justify-content-center pl-4 py-2 ${styles.sectionbar}`}
    >
      {subtitle ? (
        <Fragment>
          <h3 className={`font-weight-normal ${styles.smalltext} mb-1`}>
            {title}
          </h3>
          <h2 className={`font-weight-bold ${styles.bigtext} mb-0`}>
            {subtitle}
          </h2>
        </Fragment>
      ) : (
        <h2 className={`font-weight-bold ${styles.bigtext} mb-0`}>{title}</h2>
      )}
    </div>
  );
}
