import React, { Fragment, useState, useEffect } from "react";
import SingleDataSelectInput from "./SingleDataSelectInput";

export default function PICLine({
  id,
  name,
  task,
  description,
  availableName,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id: "",
    name: "",
    task: "",
    description: "",
  });

  useEffect(() => {
    setLine({ id, name, task, description });
  }, []);

  function updateValue(newline) {
    setLine(newline);
    onChange(newline);
  }

  function memberChanged(e) {
    console.log(e);
    const newline = { ...line, name: { id: e.value, name: e.label } };
    setLine(newline);
    onChange(newline);
  }

  function deleteLine() {
    onDelete(line);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4">
          <SingleDataSelectInput
            name="name"
            placeholder="Select pic name..."
            value={name}
            change={memberChanged}
            required="true"
            availableOptions={availableName?.map((o, i) => {
              return { value: o.value, label: o.label };
            })}
          ></SingleDataSelectInput>
        </div>

        <div className="col-4">
          <input
            className="form-control"
            id={`taskpic${id}`}
            type="text"
            name={`taskpic${id}`}
            onChange={(e) => {
              console.log(name);
              const newline = {
                ...line,
                name: { id: name.value, name: name.label },
                task: e.target.value,
              };
              updateValue(newline);
            }}
            value={line.task}
            required="true"
          />
          <div className="invalid-feedback">Field is required</div>
        </div>

        <div className="col-4">
          <textarea
            className="form-control"
            id={`descpic${id}`}
            type="text"
            name={`descpic${id}`}
            onChange={(e) => {
              const newline = {
                ...line,
                name: { id: name.value, name: name.label },
                description: e.target.value,
              };
              updateValue(newline);
            }}
            value={line.description}
            rows="1"
            required="true"
          />
          <div className="invalid-feedback">Field is required</div>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
