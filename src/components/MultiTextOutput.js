import React, { Fragment } from "react";
import styles from "./TextOutput.module.scss";

export default function MultiTextOutput({ caption, value }) {
  return (
    <Fragment>
      <p className={`col-sm-12 col-md-2 ${styles.textlabel}`}>{caption}</p>
      <div className={`col-sm-12 col-md-10 row mx-0 flex-column`}>
        {value?.map((v, i) => {
          return (
            <p key={i} className={styles.multitextcontent}>
              {v}
            </p>
          );
        })}
      </div>
    </Fragment>
  );
}
