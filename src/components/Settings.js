import React, { useState } from "react";

export default function Settings() {
  const [isActive, setActive] = useState("false");

  const toggleMenu = () => {
    setActive(!isActive);
  };

  return (
    <div class="dropdown ml-2">
      <button
        type="button"
        className="blank-button d-flex align-items-center"
        onClick={toggleMenu}
      >
        <i className="fas fa-cog" style={{ fontSize: "21px" }}></i>
      </button>
      <div className="dropdown-menu">
        <a class="dropdown-item" href="#">
          Action
        </a>
        <a class="dropdown-item" href="#">
          Another action
        </a>
        <a class="dropdown-item" href="#">
          Something else here
        </a>
      </div>
    </div>
  );
}
