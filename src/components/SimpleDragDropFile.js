import React, { Fragment, useState, useEffect, useRef } from "react";
import { FileDrop } from "react-file-drop";

export default function SimpleDragDropFile({ value, onChange, onDelete }) {
  const [thevalue, setThevalue] = useState();
  const uploadEl = useRef(null);

  // useEffect(() => {
  //   // setThevalue(value);
  // }, []);

  useEffect(() => {
    setThevalue(value);
  }, [value]);

  function onChange(e) {
    // setThevalue(e.target.value);
    // change({ key: name, value: e.target.value });
  }

  const [parentcolor, setParentcolor] = useState();
  const onDrag = (e) => {
    setParentcolor("#ececec99");
  };

  const onDragLeave = (e) => {
    setParentcolor("");
  };

  const onDrop = (files, event) => {
    setParentcolor("");
    setThevalue("https://www.7-zip.org/a/7za920.zip");
  };

  const deleteItem = () => {
    setThevalue();
  };

  return (
    <Fragment>
      <div
        className="col-sm-12 col-md-auto px-0 overflow-hidden"
        style={{ backgroundColor: parentcolor }}
      >
        {thevalue ? (
          <div className="d-flex align-items-center justify-content-start col-12 px-0">
            <div className="d-flex align-items-center justify-content-center">
              <a
                href={thevalue}
                className="d-flex align-items-center text-primary"
              >
                <i
                  className="fas fa-download"
                  style={{ fontSize: "1em", marginRight: 5 }}
                ></i>
                <span> Download</span>
              </a>
            </div>
            <div className="d-flex align-items-center justify-content-center ml-4">
              <button
                onClick={deleteItem}
                className="blank-button d-flex align-items-center text-danger"
              >
                <i className="fas fa-times" style={{ fontSize: "1em" }}></i>
              </button>
            </div>
          </div>
        ) : (
          <div
            style={{
              minWidth: 300,
              border: "solid 2px #ececec",
              borderRadius: 5,
            }}
            className="d-flex align-items-center justify-content-center col-12 py-3"
          >
            <FileDrop
              onFrameDragEnter={(event) =>
                console.log("onFrameDragEnter", event)
              }
              onFrameDragLeave={(event) =>
                console.log("onFrameDragLeave", event)
              }
              onFrameDrop={(event) => console.log("onFrameDrop", event)}
              onDragOver={onDrag}
              onDragLeave={onDragLeave}
              onDrop={onDrop}
            >
              <div className="d-flex flex-column align-items-center justify-content-center">
                <span style={{ fontSize: "0.86em", fontWeight: 600 }}>
                  Drag and Drop file here
                </span>
                <span style={{ fontSize: "0.86em" }}>or</span>
                <div className="d-flex align-items-center justify-content-center">
                  <button
                    type="button"
                    className="blank-button"
                    onClick={() => {
                      uploadEl.current.click();
                    }}
                  >
                    <a className="d-flex align-items-center text-primary">
                      <i
                        className="fas fa-upload"
                        style={{ fontSize: "1em", marginRight: 5 }}
                      ></i>
                      <span> Upload</span>
                    </a>
                  </button>

                  <input
                    hidden
                    type="file"
                    ref={uploadEl}
                    id="fileupload"
                    name="file"
                    // accept=".zip"
                    onChange={(e) =>
                      setThevalue("https://www.7-zip.org/a/7za920.zip")
                    }
                  />
                </div>
              </div>
            </FileDrop>
          </div>
        )}
      </div>
    </Fragment>
  );
}
