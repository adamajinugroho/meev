import React, { useState, useEffect } from "react";

import * as RoleStatusType from "../types/RoleStatus";
import SingleDataSelectInput from "./SingleDataSelectInput";

export default function RoleLine({
  id,
  person,
  role,
  availableName,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id,
    person,
    role,
  });

  useEffect(() => {
    setLine({ id, person, role });
  }, []);

  function deleteLine() {
    onDelete(line);
  }

  function roleChanged(e) {
    let operson = {};
    if (line.person.hasOwnProperty("value")) {
      operson = { id: line.person.value, name: line.person.label };
    } else {
      operson = line.person;
    }
    const newline = {
      ...line,
      person: operson,
      role: { value: e.value, label: e.label },
    };
    setLine(newline);
    onChange(newline);
  }

  function memberChanged(e) {
    const newline = { ...line, person: { id: e.value, name: e.label } };
    setLine(newline);
    onChange(newline);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-6">
          <SingleDataSelectInput
            name="audience"
            placeholder="Select audience..."
            value={person}
            change={memberChanged}
            required="true"
            availableOptions={availableName}
          ></SingleDataSelectInput>
        </div>
        <div className="col-6">
          <SingleDataSelectInput
            name="role"
            placeholder="Select role..."
            value={role}
            change={roleChanged}
            required="true"
            availableOptions={RoleStatusType.ROLE_STATUS_LIST.map((o, i) => {
              return { value: o, label: o };
            })}
          ></SingleDataSelectInput>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
