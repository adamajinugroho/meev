import React, { useState, useEffect } from "react";

import * as RoleStatusType from "../types/RoleStatus";
import ChipNoGroupSelectInput from "./ChipNoGroupSelectInput";
import SingleDataSelectInput from "./SingleDataSelectInput";

export default function GroupLine({
  id,
  name,
  description,
  members,
  availableName,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id: "",
    id_person: "",
    name_person: "",
    role: "",
  });

  useEffect(() => {
    setLine({ id, name, description, members });
  }, []);

  function deleteLine() {
    onDelete(line);
  }

  function memberChanged(e) {
    const newline = {
      ...line,
      members: e.map((v) => ({ id: v.value, name: v.label })),
    };

    setLine(newline);
    onChange(newline);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4">
          <input
            className="form-control"
            id={`groupname${id}`}
            type="text"
            name={`groupname${id}`}
            onChange={(e) => {
              const newline = { ...line, name: e.target.value };
              setLine(newline);
              onChange(newline);
            }}
            value={name}
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>
        <div className="col-4">
          <input
            className="form-control"
            id={`groupdesc${id}`}
            type="text"
            name={`groupdesc${id}`}
            onChange={(e) => {
              const newline = { ...line, description: e.target.value };
              setLine(newline);
              onChange(newline);
            }}
            value={description}
            required
          />
        </div>
        <div className="col-4">
          <ChipNoGroupSelectInput
            name="groupmembers"
            placeholder="Select member..."
            value={members}
            change={memberChanged}
            required="true"
            availableOptions={availableName}
          ></ChipNoGroupSelectInput>
          <div className="invalid-feedback">Field is required</div>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
