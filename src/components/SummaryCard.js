import React, { Fragment } from "react";
import styles from "./SummaryCard.module.scss";

export default function SummaryCard({ caption, value, image, color, mcorr }) {
  return (
    <div
      className={`d-flex align-items-center px-4 py-0 justify-content-between ${styles.card} w-100`}
      // w-100
      style={{
        backgroundColor: color,
        minHeight: 80,
        // minWidth: 300,
        // boxShadow: " 0 4px 8px 0 rgba(0,0,0,0.2)",
        // width: "200px",
      }}
    >
      <div style={{ marginRight: 20 }}>
        <p className={`font-weight-bold ${styles.value}`}>{value}</p>
        <p className={`${styles.title}`}>{caption}</p>
      </div>
      <div
        // className="rounded-circle bg-light d-flex justify-content-center align-items-center"
        className="d-flex justify-content-center align-items-center"
        style={{ width: 50, height: 50, color: color }}
      >
        <div className=" d-flex justify-content-center align-items-center">
          <img
            style={{ width: 35, maxHeight: 40, marginLeft: mcorr }}
            src={image}
          ></img>
          {/* <svg focusable="false" viewBox="0 0 640 512">
            <path
              fill="currentColor"
              d="M496 224c-79.59 0-144 64.41-144 144s64.41 144 144 144 144-64.41 144-144-64.41-144-144-144zm64 150.29c0 5.34-4.37 9.71-9.71 9.71h-60.57c-5.34 0-9.71-4.37-9.71-9.71v-76.57c0-5.34 4.37-9.71 9.71-9.71h12.57c5.34 0 9.71 4.37 9.71 9.71V352h38.29c5.34 0 9.71 4.37 9.71 9.71v12.58zM496 192c5.4 0 10.72.33 16 .81V144c0-25.6-22.4-48-48-48h-80V48c0-25.6-22.4-48-48-48H176c-25.6 0-48 22.4-48 48v48H48c-25.6 0-48 22.4-48 48v80h395.12c28.6-20.09 63.35-32 100.88-32zM320 96H192V64h128v32zm6.82 224H208c-8.84 0-16-7.16-16-16v-48H0v144c0 25.6 22.4 48 48 48h291.43C327.1 423.96 320 396.82 320 368c0-16.66 2.48-32.72 6.82-48z"
            ></path>
          </svg> */}
        </div>
      </div>
    </div>
  );
}
