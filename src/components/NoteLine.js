import React, { Fragment, useState, useEffect } from "react";

export default function NoteLine({ id, point, notes, onDelete, onChange }) {
  const [line, setLine] = useState({
    id: "",
    point: "",
    notes: "",
  });

  useEffect(() => {
    setLine({ id, point, notes });
  }, []);

  function updateValue() {
    onChange(line);
  }

  function deleteLine() {
    onDelete(line);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4">
          <input
            className="form-control"
            id={`pointnote${id}`}
            type="text"
            name={`pointnote${id}`}
            onChange={(e) => {
              setLine({ ...line, point: e.target.value });
              updateValue();
            }}
            value={line.point}
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>

        <div className="col-8">
          <textarea
            className="form-control"
            id={`descnote${id}`}
            type="text"
            name={`descnote${id}`}
            onChange={(e) => {
              setLine({ ...line, notes: e.target.value });
              updateValue();
            }}
            value={line.notes}
            rows="1"
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
