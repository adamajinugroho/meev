import React, { Fragment, useState, useEffect } from "react";
import "react-widgets/dist/css/react-widgets.css";
import { DateTimePicker } from "react-widgets";
import Moment from "moment";
import styles from "./SimpleDatetimePicker.module.scss";

export default function SimpleDatetimePicker({
  caption,
  name,
  value,
  initval,
  change,
  placeholder,
  required,
  disableTime,
}) {
  const [thevalue, setThevalue] = useState();

  // useEffect(() => {
  //   const mdate = Moment();
  //   // console.log("initval", initval);
  //   // console.log("value", Moment(new Date(value)).toDate());
  //   if (initval) {
  //     const d = initval.split("-");
  //     mdate.set({ year: +d[0], month: +d[1] - 1, date: +d[2] });
  //     console.log(mdate.toDate());
  //     setThevalue(mdate.toDate());
  //   } else if (value) {
  //     setThevalue(new Date(value));
  //   } else {
  //     // setThevalue(mdate.toDate());
  //   }
  // }, []);

  useEffect(() => {
    const mdate = Moment();
    // console.log("initval", initval);
    // console.log("value", Moment(new Date(value)).toDate());
    if (initval) {
      const d = initval.split("-");
      mdate.set({ year: +d[0], month: +d[1] - 1, date: +d[2] });
      console.log(mdate.toDate());
      setThevalue(mdate.toDate());
    } else if (value) {
      setThevalue(new Date(value));
    }
  }, [initval, value]);

  function onChange(value) {
    console.log(value);
    setThevalue(value);
    change({ key: name, value: value?.toString() });
  }

  return (
    <Fragment>
      {caption ? (
        <label
          className="col-sm-12 col-md-2"
          style={{ marginTop: "0.5em" }}
          for={name}
        >
          {caption}
          {required ? <span className="text-danger ml-0">*</span> : ""}
        </label>
      ) : (
        ""
      )}
      <div className="col-sm-12 col-md-auto">
        <DateTimePicker
          id={name}
          name={name}
          onChange={onChange}
          value={thevalue}
          placeholder={placeholder}
          format={disableTime ? "DD-MM-YYYY" : "DD-MM-YYYY HH:mm"}
          time={!disableTime}
        />
        {required && !thevalue ? (
          <div className="invalid-feedback custom-invalid">
            Field is required
          </div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
