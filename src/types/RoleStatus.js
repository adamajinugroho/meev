export const ADMIN = "Admin";
export const EC = "Event Creator";
export const MEMBER = "Member";
export const ROLE_STATUS_LIST = [ADMIN, EC];
export const ROLE_ALL_STATUS_LIST = [...ROLE_STATUS_LIST, MEMBER];
