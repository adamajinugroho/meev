export const DONE = "Done";
export const CANCEL = "Cancel";
export const NYS = "Scheduled";
export const MEETING_STATUS_LIST = [DONE, CANCEL, NYS];
