export const CHAIRMAN = "Chairman";
export const MEETING = "Meeting";
export const PIC = "PIC";
export const TASK = "Task";
export const ACTIVITY_STATUS_LIST = [CHAIRMAN, MEETING, PIC, TASK];
