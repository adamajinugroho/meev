import React from "react";

const Dashboard = React.lazy(() => import("./pages/Dashboard"));
const CEMeeting = React.lazy(() => import("./pages/CEMeeting"));
const CEReport = React.lazy(() => import("./pages/CEReport"));
const Meeting = React.lazy(() => import("./pages/Meeting"));
const MeetingDetails = React.lazy(() => import("./pages/MeetingDetails"));
const Tasks = React.lazy(() => import("./pages/Tasks"));
const Audiences = React.lazy(() => import("./pages/Audiences"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/meeting", name: "Meeting", component: Meeting, exact: true },
  {
    path: "/meeting/create",
    name: "Create Meeting",
    component: CEMeeting,
    exact: true,
  },
  {
    path: "/meeting/create/:datetime",
    name: "Create Meeting",
    component: CEMeeting,
  },
  { path: "/meeting/edit/:id", name: "Edit Meeting", component: CEMeeting },
  {
    path: "/meeting/detail/:id",
    name: "Detail Meeting",
    component: MeetingDetails,
  },
  { path: "/meeting/report", name: "Report", component: CEReport },
  { path: "/tasks", name: "Tasks", component: Tasks, exact: true },
  { path: "/audiences", name: "Audiences", component: Audiences, exact: true },
];

export default routes;
