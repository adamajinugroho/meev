import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import SectionBar from "../components/SectionBar";
import * as TasksStatusType from "../types/TasksStatus";
import * as GeneralStatusType from "../types/GeneralStatus";
import TasksTableLine from "../components/TasksTableLine";

export default function Tasks() {
  function textChanged(e) {
    console.log(e);
  }

  function taskListLineChanged(line) {
    // const lines = [...picList.filter((l) => +l.id !== +line.id), line];
    // setPicList(lines);
  }

  function taskListLineDeleted(id) {
    // const lines = decisionList.filter((l) => +l.id !== +id);
    // setDecisionList(lines);
  }

  useEffect(() => {
    // console.log(JSON.stringify(localData));
  });

  const [taskList, setTaskList] = useState([
    {
      id: "1",
      name: "Regulasi Frekuensi",
      description: "create presentation",
      evidence: "",
      status: TasksStatusType.ON_PROGRESS,
      approved: GeneralStatusType.NO,
      due: "Tue Dec 16 2020 12:53:44 GMT+0700 (Indochina Time)",
      creator: true,
    },
    {
      id: "2",
      name: "Regulasi Frekuensi",
      description: "follow up presentation",
      evidence: "",
      status: TasksStatusType.NY,
      approved: GeneralStatusType.NO,
      due: "Tue Dec 17 2020 12:53:44 GMT+0700 (Indochina Time)",
      creator: true,
    },
    {
      id: "3",
      name: "Regulasi Frekuensi",
      description: "presentation",
      evidence:
        "https://www.gravatar.com/avatar/f9f64a49c04ab42bd1c59c76b76ef45c?s=32&d=identicon&r=PG",
      status: TasksStatusType.DONE,
      approved: GeneralStatusType.YES,
      due: "Tue Dec 18 2020 12:53:44 GMT+0700 (Indochina Time)",
      creator: false,
    },
  ]);

  return (
    <Fragment>
      <SectionBar title="Tasks"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <div className="container-fluid px-4">
            <div className="row spacing-group mx-sm-4">
              <h4 className="title-group col-sm-12 col-md-4 my-2">Task List</h4>
              <div className="col-sm-12 col-md-8 my-2 d-flex align-items-center justify-content-md-end justify-content-sm-start">
                {/* <Link
                  to="/task/create"
                  className="px-0 blank-button text-primary d-flex align-items-center justify-content-end"
                >
                  <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                  <span className="add" style={{ marginLeft: "5px" }}>
                    Add new data
                  </span>
                </Link> */}
              </div>
            </div>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list2">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-2 column-list">NAME</div>
                  <div className="col-3 column-list">DESCRIPTION</div>
                  <div className="col-2 column-list">DUE</div>
                  <div className="col-2 column-list">EVIDENCE</div>
                  <div className="col-2 column-list">STATUS</div>
                  <div className="col-1 column-list d-flex justify-content-center">
                    APPROVED
                  </div>
                </div>
              </div>

              {taskList.map(
                (
                  {
                    id,
                    name,
                    description,
                    evidence,
                    status,
                    due,
                    approved,
                    creator,
                  },
                  i
                ) => (
                  <TasksTableLine
                    id={id}
                    name={name}
                    description={description}
                    evidence={evidence}
                    due={due}
                    approved={approved}
                    status={status}
                    creator={creator}
                    key={i}
                    onChange={taskListLineChanged}
                    onDelete={taskListLineDeleted}
                  />
                )
              )}
            </div>
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
