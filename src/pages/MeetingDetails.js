import React, { Fragment, useState, useEffect } from "react";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import PICLine from "../components/PICLine";
import SimpleTextOutput from "../components/SimpleTextOutput";
import MultiTextOutput from "../components/MultiTextOutput";
import PICDetailsLine from "../components/PICDetailsLine";

export default function MeetingDetails() {
  useEffect(() => {
    // Fetch from API
    setLocalData({
      event: "Regulasi frekuensi",
      chairman: "Budi",
      agenda: "Pembahasan regulasi",
      offlineloc: "Gedung A",
      onlineloc: "https://meeet.com/ava2as21kfa",
      attachment: "https://www.7-zip.org/a/7za920.zip",
      datetime: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
      availableNames: {
        personOptions: [
          { value: "person 1", label: "Person 1", color: "#4B4B4D" },
          { value: "person 2", label: "Person 2", color: "#4B4B4D" },
          { value: "person 3", label: "Person 3", color: "#4B4B4D" },
        ],
        groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
        externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
      },
      attendees: [
        { id: "person 1", name: "Person 1", color: "#4B4B4D" },
        { id: "person 2", name: "Person 2", color: "#4B4B4D" },
        { id: "person 3", name: "Person 3", color: "#4B4B4D" },
        { id: "2", name: "Group", color: "#36B37E" },
        { id: "5", name: "External", color: "#00B8D9" },
      ],
    });

    setPicList([
      {
        id: "1",
        name: {
          id: "person 1",
          name: "Person 1",
        },
        task: "task 1",
        description: "detail task 1",
      },
      {
        id: "2",
        name: {
          id: "person 2",
          name: "Person 2",
        },
        task: "task 2",
        description: "detail task 2",
      },
      {
        id: "3",
        name: {
          id: "person 3",
          name: "Person 3",
        },
        task: "task 2",
        description: "detail task 3",
      },
    ]);
  }, []);

  useEffect(() => {
    console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState();

  const [picList, setPicList] = useState([]);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle="Meeting Details"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <div className="container-fluid px-4">
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Event"
                  value={localData?.event}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Chairman"
                  value={localData?.chairman}
                ></SimpleTextOutput>
              </div>
              {localData?.locoffline ? (
                <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                  <SimpleTextOutput
                    caption="Location"
                    value={localData?.locoffline}
                  ></SimpleTextOutput>
                </div>
              ) : (
                ""
              )}
              {localData?.loconline ? (
                <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                  <SimpleTextOutput
                    caption="Online URL"
                    value={localData?.loconline}
                    link="true"
                  ></SimpleTextOutput>
                </div>
              ) : (
                ""
              )}
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Datetime"
                  value="20-03-2020 10:30"
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Agenda"
                  value={localData?.agenda}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <MultiTextOutput
                  caption="Attendees"
                  value={localData?.attendees?.map((a) => a.name)}
                ></MultiTextOutput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">PIC List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-12">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Task</div>
                  <div className="col-4 column-list">Description</div>
                </div>
              </div>

              {picList?.map((object, i) => (
                <PICDetailsLine
                  id={object.id}
                  name={object.name.name}
                  task={object.task}
                  description={object.description}
                  key={i}
                />
              ))}
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Attachment</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                {localData?.attachment ? (
                  <div class="d-flex align-items-center justify-content-start col-12">
                    <a
                      href={localData?.attachment}
                      className="d-flex align-items-center text-primary"
                    >
                      <i
                        className="fas fa-download"
                        style={{ fontSize: "1em", marginRight: 5 }}
                      ></i>
                      <span> Download</span>
                    </a>
                  </div>
                ) : (
                  <span className="col-12">None</span>
                )}
              </div>
            </div>

            <div style={{ marginBottom: 150 }}></div>
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
