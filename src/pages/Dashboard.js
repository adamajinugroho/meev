import React, { Fragment, useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import SectionBar from "../components/SectionBar";
import SummaryCard from "../components/SummaryCard";
import crown from "../assets/crown-solid.svg";
import clipboard from "../assets/clipboard-solid.svg";
import business from "../assets/business-time-solid.svg";
import filesig from "../assets/file-signature-solid.svg";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import * as ActivityStatusType from "../types/ActivityStatus";
import AdaptiveWithDetector from "../components/AdaptiveWithDetector";

function Dashboard() {
  const chairmanColor = "#c7b699"; //#321FDB";
  const meetingColor = "#414754"; //#3399FF";
  const picColor = "#82807F"; // "#F9B115";
  const taskColor = "#aaaeb5"; //"#E55353";

  const calendar = useRef(null);

  useEffect(() => {
    // API Fetch
    setEventList({
      summary: {
        chairman: 1,
        meeting: 2,
        pic: 1,
        task: 4,
      },
      event: [
        {
          id: "1",
          type: ActivityStatusType.MEETING,
          title: "event 1",
          date: "2020-12-01",
          color: meetingColor,
        },
        {
          id: "2",
          type: ActivityStatusType.MEETING,
          title: "event 2",
          date: "2020-12-02",
          color: meetingColor,
        },
        {
          id: "1",
          type: ActivityStatusType.TASK,
          title: "task name",
          date: "2020-12-02",
          color: taskColor,
        },
        {
          id: "2",
          type: ActivityStatusType.PIC,
          title: "task name",
          date: "2020-12-16",
          color: picColor,
        },
        {
          id: "3",
          type: ActivityStatusType.CHAIRMAN,
          title: "event",
          date: "2020-12-20",
          color: chairmanColor,
        },
      ],
    });
  }, []);

  let history = useHistory();
  const [eventList, setEventList] = useState({});

  const handleDateClick = (arg) => {
    console.log(arg.dateStr);
    history.push("/meeting/create/" + arg.dateStr);
  };

  const handleEventClick = (arg) => {
    if (
      arg.event.extendedProps.type === ActivityStatusType.CHAIRMAN ||
      arg.event.extendedProps.type === ActivityStatusType.MEETING
    ) {
      history.push("/meeting/detail/" + arg.event.id);
    } else {
      history.push("/tasks");
    }
  };

  const forceRefreshCalendar = () => {
    const api = calendar.current.getApi();
    api.updateSize();
  };

  return (
    <Fragment>
      <SectionBar title="Dashboard"></SectionBar>
      <div className="wrapper mt-4">
        <div className="base-content" style={{ backgroundColor: "#FFFFFF00" }}>
          <div className="container-fluid px-4">
            <div className="d-flex flex-column mx-sm-4 mb-4">
              <div className="col-12 px-0 row mx-0 mb-4 justify-content-around">
                <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Meeting Chairman"
                    value={eventList.summary?.chairman || 0}
                    color={chairmanColor}
                    image={crown}
                  ></SummaryCard>
                </div>
                <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Meeting Invitation"
                    value={eventList.summary?.meeting || 0}
                    color={meetingColor}
                    image={business}
                    mcorr={3}
                  ></SummaryCard>
                </div>
                <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="PIC Agenda"
                    value={eventList.summary?.pic || 0}
                    color={picColor}
                    image={filesig}
                    mcorr={5}
                  ></SummaryCard>
                </div>
                <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Tasks To Do"
                    value={eventList.summary?.task || 0}
                    color={taskColor}
                    image={clipboard}
                  ></SummaryCard>
                </div>
              </div>

              <div className="card p-4">
                <AdaptiveWithDetector callback={forceRefreshCalendar}>
                  <FullCalendar
                    key={"asdvlaksgjasldh"}
                    ref={calendar}
                    plugins={[dayGridPlugin, interactionPlugin]}
                    initialView="dayGridMonth"
                    dateClick={handleDateClick}
                    eventClick={handleEventClick}
                    events={eventList.event || []}
                  />
                </AdaptiveWithDetector>
              </div>
            </div>
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}

export default Dashboard;
