import React, { Fragment, useState, useEffect, useRef } from "react";
import styles from "./CEMeeting.module.scss";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import PICLine from "../components/PICLine";
import ChipSelectInput from "../components/ChipSelectInput";
import SimpleDatetimePicker from "../components/SimpleDatetimePicker";
import { useHistory, useParams } from "react-router-dom";
import { FileDrop } from "react-file-drop";
import SimpleDragDropFile from "../components/SimpleDragDropFile";

export default function CEMeeting(props) {
  let { datetime } = useParams();
  let history = useHistory();
  let mainform = useRef(null);

  useEffect(() => {
    console.log(datetime);
    if (datetime) {
      setLocalData({ ...localData, datetime });
    }

    // API Fetch
    // ...

    //Edit
    const path = props.location.pathname.split("/")[2];
    if (path === "edit") {
      setSubtitle("Edit Existing Meeting");
      setLocalData({
        event: "Regulasi frekuensi",
        chairman: "Budi",
        agenda: "Pembahasan regulasi",
        offlineloc: "Gedung A",
        onlineloc: "https://meeet.com/ava2as21kfa",
        attachment: "https://www.7-zip.org/a/7za920.zip",
        datetime: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
        availableNames: {
          personOptions: [
            { value: "person 1", label: "Person 1", color: "#4B4B4D" },
            { value: "person 2", label: "Person 2", color: "#4B4B4D" },
            { value: "person 3", label: "Person 3", color: "#4B4B4D" },
          ],
          groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
          externalOptions: [
            { value: "5", label: "External", color: "#00B8D9" },
          ],
        },
        candidateNames: [
          { id: "person 1", name: "Person 1", color: "#4B4B4D" },
          { id: "person 2", name: "Person 2", color: "#4B4B4D" },
          { id: "person 3", name: "Person 3", color: "#4B4B4D" },
          { id: "2", name: "Group", color: "#36B37E" },
          { id: "5", name: "External", color: "#00B8D9" },
        ],
      });

      setPicList([
        {
          id: "1",
          name: {
            id: "person 1",
            name: "Person 1",
          },
          task: "task 1",
          description: "detail task 1",
        },
        {
          id: "2",
          name: {
            id: "person 2",
            name: "Person 2",
          },
          task: "task 2",
          description: "detail task 2",
        },
        {
          id: "3",
          name: {
            id: "person 3",
            name: "Person 3",
          },
          task: "task 2",
          description: "detail task 3",
        },
      ]);
    } else {
      setSubtitle("Create New Meeting");
      setLocalData({
        availableNames: {
          personOptions: [
            { value: "person 1", label: "Person 1", color: "#4B4B4D" },
            { value: "person 2", label: "Person 2", color: "#4B4B4D" },
          ],
          groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
          externalOptions: [
            { value: "5", label: "External", color: "#00B8D9" },
          ],
        },
        candidateNames: [],
      });
    }
  }, []);

  function textChanged(e) {
    console.log(e);
  }

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function picLineChanged(line) {
    const lines = picList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...picList.filter((l) => +l.id !== +line.id), line];
    setPicList(lines);
  }

  function picLineDeleted(line) {
    const lines = picList.filter((l) => +l.id !== +line.id);
    setPicList(lines);
  }

  function picLineAdded() {
    setPicList([
      ...picList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        name: {
          id: "",
          name: "",
        },
        task: "",
        description: "",
      },
    ]);
  }

  function audiencesChanged(e) {
    setLocalData({ ...localData, candidateNames: e });
    setAvailableName(e);
  }

  useEffect(() => {
    console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState();

  const [picList, setPicList] = useState([]);

  const [availableName, setAvailableName] = useState([]);

  const [subtitle, setSubtitle] = useState();

  const [wasvalidated, setWasvalidated] = useState(false);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle={subtitle}></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            ref={mainform}
            className={`container-fluid px-4 ${
              wasvalidated ? "was-validated" : ""
            }`}
            noValidate
          >
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextInput
                  name="event"
                  caption="Event"
                  placeholder=""
                  value={localData?.event}
                  change={dataChanged}
                  required="true"
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextInput
                  name="chairman"
                  caption="Chairman"
                  placeholder=""
                  value={localData?.chairman}
                  change={dataChanged}
                  required="true"
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <LocationTextInput
                  offlineloc={localData?.offlineloc}
                  onlineloc={localData?.onlineloc}
                  change={textChanged}
                ></LocationTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleDatetimePicker
                  name="datetime"
                  caption="Datetime"
                  placeholder="Select datetime"
                  initval={datetime}
                  value={localData?.datetime}
                  change={dataChanged}
                  required="true"
                ></SimpleDatetimePicker>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <AreaTextInput
                  name="agenda"
                  caption="Agenda"
                  placeholder=""
                  value={localData?.agenda}
                  change={dataChanged}
                  rows="5"
                  required="true"
                ></AreaTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <ChipSelectInput
                  name="attendees"
                  caption="Attendees"
                  placeholder="Invite audiences..."
                  value={localData?.candidateNames}
                  change={audiencesChanged}
                  required="true"
                  availableNames={localData?.availableNames}
                ></ChipSelectInput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">PIC List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Task</div>
                  <div className="col-4 column-list">Description</div>
                </div>
              </div>

              {picList?.map((object, i) => (
                <PICLine
                  id={object.id}
                  // name={object.name}
                  name={{
                    value: object.name.id,
                    label: object.name.name,
                  }}
                  task={object.task}
                  description={object.description}
                  key={object.id}
                  // key={Math.floor(Math.random() * 10000)}
                  onChange={picLineChanged}
                  onDelete={picLineDeleted}
                  // availableName={localData?.availableNames?.personOptions}
                  availableName={localData?.candidateNames?.map((o, i) => {
                    return { value: o.id, label: o.name };
                  })}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={picLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Attachment</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0 mx-0">
                <SimpleDragDropFile
                  value={localData?.attachment}
                ></SimpleDragDropFile>
              </div>
            </div>

            <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                class="btn btn-custom btn-secondary mr-3"
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/meeting");
                }}
              >
                Cancel
              </button>
              <button
                class="btn btn-custom btn-primary-custom"
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  setWasvalidated(true);
                  if (mainform.current.checkValidity()) {
                    history.push("/meeting");
                  }
                }}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
