import React, { Fragment, useState, useEffect } from "react";
import styles from "./CEMeeting.module.scss";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import PICLine from "../components/PICLine";
import * as MeetingStatusType from "../types/MeetingStatus";
import SimpleComboBox from "../components/SimpleComboBox";
import NoteLine from "../components/NoteLine";
import DecisionLine from "../components/DecisionLine";
import ChipSelectInput from "../components/ChipSelectInput";
import SingleDataSelectInput from "../components/SingleDataSelectInput";
import { useHistory } from "react-router-dom";

export default function CEReport() {
  let history = useHistory();

  useEffect(() => {
    // API Fetch
    // ...

    setLocalData({
      status: { value: 1, label: MeetingStatusType.DONE },
      availableNames: {
        personOptions: [
          { value: "person 1", label: "Person 1", color: "#4B4B4D" },
          { value: "person 2", label: "Person 2", color: "#4B4B4D" },
        ],
        groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
        externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
      },
      attendees: [
        { id: "person 1", name: "Person 1", color: "#4B4B4D" },
        { id: "2", name: "Group", color: "#36B37E" },
        { id: "5", name: "External", color: "#00B8D9" },
      ],
    });

    setDecisionList([
      {
        id: "1",
        decision: "decision 1",
        pic: {
          id: "person 1",
          name: "Person 1",
        },
        detail: "detail 1",
        due: "Tue Dec 16 2020 12:53:44 GMT+0700 (Indochina Time)",
      },
      {
        id: "2",
        decision: "decision 2",
        pic: {
          id: "person 2",
          name: "Person 2",
        },
        detail: "detail 2",
        due: "Tue Dec 29 2020 12:53:44 GMT+0700 (Indochina Time)",
      },
    ]);

    setNoteList([
      {
        id: "1",
        point: "point 1",
        notes: "notes 1",
      },
      {
        id: "2",
        point: "point 2",
        notes: "notes 2",
      },
    ]);
  }, []);

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function noteLineChanged(line) {
    const lines = noteList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...noteList.filter((l) => +l.id !== +line.id), line];
    setNoteList(lines);
  }

  function noteLineDeleted(line) {
    const lines = noteList.filter((l) => +l.id !== +line.id);
    setNoteList(lines);
  }

  function noteLineAdded() {
    setNoteList([
      ...noteList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        point: "",
        notes: "",
      },
    ]);
  }

  function decisionLineChanged(line) {
    const lines = decisionList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...decisionList.filter((l) => +l.id !== +line.id), line];
    setDecisionList(lines);
  }

  function decisionLineDeleted(id) {
    const lines = decisionList.filter((l) => +l.id !== +id);
    setDecisionList(lines);
  }

  function decisionLineAdded() {
    setDecisionList([
      ...decisionList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        decision: "",
        pic: {
          id: "",
          name: "",
        },
        detail: "",
      },
    ]);
  }

  function audiencesChanged(e) {
    console.log(e);
    setLocalData({ ...localData, attendees: e });
  }

  useEffect(() => {
    // console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState();

  const [noteList, setNoteList] = useState([]);

  const [decisionList, setDecisionList] = useState([]);

  const [wasvalidated, setWasvalidated] = useState(false);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle="Submit Report"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            className={`container-fluid px-4 ${
              wasvalidated ? "was-validated" : ""
            }`}
            noValidate
          >
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SingleDataSelectInput
                  name="status"
                  caption="Status"
                  placeholder="Select meeting status..."
                  value={localData?.status}
                  change={dataChanged}
                  required="true"
                  availableOptions={MeetingStatusType.MEETING_STATUS_LIST.map(
                    (o, i) => {
                      return { value: i + 1, label: o };
                    }
                  )}
                ></SingleDataSelectInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <ChipSelectInput
                  name="attendees"
                  caption="Attendees"
                  placeholder="attendee list..."
                  value={localData?.attendees}
                  change={audiencesChanged}
                  required="true"
                  availableNames={localData?.availableNames}
                ></ChipSelectInput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Note List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Point / Case</div>
                  <div className="col-8 column-list">Notes</div>
                </div>
              </div>

              {noteList?.map((object, i) => (
                <NoteLine
                  id={object.id}
                  point={object.point}
                  notes={object.notes}
                  key={object.id}
                  onChange={noteLineChanged}
                  onDelete={noteLineDeleted}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={noteLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Decision List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-3 column-list">PIC</div>
                  <div className="col-3 column-list">Decision</div>
                  <div className="col-3 column-list">Detail</div>
                  <div className="col-3 column-list">Due</div>
                </div>
              </div>

              {decisionList?.map((object, i) => (
                <DecisionLine
                  id={object.id}
                  pic={{ value: object.pic.id, label: object.pic.name }}
                  decision={object.decision}
                  detail={object.detail}
                  due={object.due}
                  key={object.id}
                  onChange={decisionLineChanged}
                  onDelete={decisionLineDeleted}
                  availableName={localData.availableNames.personOptions}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={decisionLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                class="btn btn-custom btn-secondary mr-3"
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/meeting");
                }}
              >
                Cancel
              </button>
              <button
                class="btn btn-custom btn-primary-custom"
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  setWasvalidated(true);
                }}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
