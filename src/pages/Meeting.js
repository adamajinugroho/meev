import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import MeetingTableLine from "../components/MeetingTableLine";
import SectionBar from "../components/SectionBar";
import * as MeetingStatusType from "../types/MeetingStatus";

export default function Meeting() {
  function textChanged(e) {
    console.log(e);
  }

  function meetListLineChanged(line) {
    // const lines = [...picList.filter((l) => +l.id !== +line.id), line];
    // setPicList(lines);
  }

  function meetListLineDeleted(id) {
    // const lines = decisionList.filter((l) => +l.id !== +id);
    // setDecisionList(lines);
  }

  useEffect(() => {
    // console.log(JSON.stringify(localData));
  });

  const [meetingStatus, setMeetingStatus] = useState(MeetingStatusType.NYS);

  const [meetList, setMeetList] = useState([
    {
      id: "1",
      event: "Regulasi Frekuensi",
      date: "03-03-2020",
      time: "10:30",
      pic: "Yes",
      attachment:
        "https://www.gravatar.com/avatar/f9f64a49c04ab42bd1c59c76b76ef45c?s=32&d=identicon&r=PG",
      status: MeetingStatusType.DONE,
    },
    {
      id: "2",
      event: "Regulasi Frekuensi",
      date: "03-03-2020",
      time: "10:30",
      pic: "No",
      attachment: "",
      status: MeetingStatusType.NYS,
    },
  ]);

  return (
    <Fragment>
      <SectionBar title="Meeting"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <div className="container-fluid px-4">
            <div className="row spacing-group mx-sm-4">
              <h4 className="title-group col-sm-12 col-md-4 my-2">
                Meeting List
              </h4>
              <div className="col-sm-12 col-md-8 my-2 d-flex align-items-center justify-content-md-end justify-content-sm-start">
                <Link
                  to="/meeting/create"
                  className="px-0 blank-button text-primary d-flex align-items-center justify-content-end"
                >
                  <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                  <span className="add" style={{ marginLeft: "5px" }}>
                    Add new data
                  </span>
                </Link>
              </div>
            </div>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list2">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-3 column-list">EVENT</div>
                  <div className="col-2 column-list">DATE</div>
                  <div className="col-2 column-list">TIME</div>
                  <div className="col-2 column-list">AS PIC</div>
                  <div className="col-2 column-list">ATTACHMENT</div>
                  <div className="col-1 column-list">STATUS</div>
                </div>
              </div>

              {meetList.map(
                ({ id, event, date, time, pic, attachment, status }, i) => (
                  <MeetingTableLine
                    id={id}
                    event={event}
                    date={date}
                    time={time}
                    pic={pic}
                    attachment={attachment}
                    status={status}
                    key={i}
                    onChange={meetListLineChanged}
                    onDelete={meetListLineDeleted}
                  />
                )
              )}
            </div>
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
