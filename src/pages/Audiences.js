import React, { Fragment, useState, useEffect } from "react";
import styles from "./CEMeeting.module.scss";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import SimpleComboBox from "../components/SimpleComboBox";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import RoleLine from "../components/RoleLine";
import ChipSelectInput from "../components/ChipSelectInput";
import * as RoleStatusType from "../types/RoleStatus";
import ExternalLine from "../components/ExternalLine";
import GroupLine from "../components/GroupLine";
import { useHistory } from "react-router-dom";

export default function Audiences() {
  let history = useHistory();

  useEffect(() => {
    // API Fetch
  }, []);

  function roleLineChanged(line) {
    const lines = roleList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    console.log(lines);
    // const lines = [...roleList.filter((l) => +l.id !== +line.id), line];
    setRoleList(lines);
  }

  function roleLineDeleted(line) {
    const lines = roleList.filter((l) => +l.id !== +line.id);
    setRoleList(lines);
  }

  function roleLineAdded() {
    setRoleList([
      ...roleList,
      {
        id: Math.floor(Math.random(0, 1) * 1000).toString(),
        id_person: "",
        name_person: "",
        role: RoleStatusType.EC,
      },
    ]);
  }

  function groupLineChanged(line) {
    const lines = groupList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...groupList.filter((l) => +l.id !== +line.id), line];
    setGroupList(lines);
  }

  function groupLineDeleted(line) {
    const lines = groupList.filter((l) => +l.id !== +line.id);
    setGroupList(lines);
  }

  function groupLineAdded() {
    setGroupList([
      ...groupList,
      {
        id: Math.floor(Math.random(0, 1) * 1000).toString(),
        name: "",
        description: "",
        members: [],
      },
    ]);
  }

  function externalLineChanged(line) {
    const lines = externalList.map((l) => {
      if (+l.id === +line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...externalList.filter((l) => +l.id !== +line.id), line];
    setExternalList(lines);
  }

  function externalLineDeleted(line) {
    const lines = externalList.filter((l) => +l.id !== +line.id);
    setExternalList(lines);
  }

  function externalLineAdded() {
    setExternalList([
      ...externalList,
      {
        id: Math.floor(Math.random(0, 1) * 1000).toString(),
        name: "",
        information: "",
        contact: "",
      },
    ]);
  }

  useEffect(() => {
    console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState({
    availableNames: {
      personOptions: [
        { value: "person 1", label: "Person 1" },
        { value: "person 2", label: "Person 2" },
      ],
      groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
      externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
    },
  });

  const dummyRole = [
    {
      id: "1",
      person: { id: "person 1", name: "Person 1" },
      role: { value: RoleStatusType.ADMIN, label: RoleStatusType.ADMIN },
    },
    {
      id: "2",
      person: { id: "person 2", name: "Person 2" },
      role: { value: RoleStatusType.EC, label: RoleStatusType.EC },
    },
  ];
  const [roleList, setRoleList] = useState(dummyRole);

  const dummyGroup = [
    {
      id: "1",
      name: "Software Engineers",
      description: "Tim project A",
      members: [
        { id: "1", name: "Person 1", color: "#4B4B4D" },
        { id: "2", name: "Person 2", color: "#4B4B4D" },
      ],
    },
    {
      id: "2",
      name: "Document Admin",
      description: "Reporter",
      members: [{ id: "1", name: "Person 1" }],
    },
  ];
  const [groupList, setGroupList] = useState(dummyGroup);

  const dummyExternal = [
    {
      id: "1",
      name: "Yudi",
      information: "PT ABS",
      contact: "0853999999",
    },
    {
      id: "2",
      name: "Yana",
      information: "PT BCA",
      contact: "08539789999",
    },
  ];
  const [externalList, setExternalList] = useState(dummyExternal);

  const [wasvalidated, setWasvalidated] = useState(false);

  return (
    <Fragment>
      <SectionBar
        title="Audiences"
        subtitle="Configure Audiences Setting"
      ></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            className={`container-fluid px-4 ${
              wasvalidated ? "was-validated" : ""
            }`}
            noValidate
          >
            <h4 className="mt-4 grouptext ml-sm-4">Role Setting</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-6 column-list">Name</div>
                  <div className="col-6 column-list">Privilege</div>
                </div>
              </div>
              {roleList?.map((object, i) => (
                <RoleLine
                  id={object.id}
                  person={{
                    value: object.person.id,
                    label: object.person.name,
                  }}
                  role={object.role}
                  key={object.id}
                  onChange={roleLineChanged}
                  onDelete={roleLineDeleted}
                  availableName={localData?.availableNames?.personOptions}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={roleLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Group Audiences</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Description</div>
                  <div className="col-4 column-list">Members</div>
                </div>
              </div>

              {groupList?.map((object, i) => (
                <GroupLine
                  id={object.id}
                  name={object.name}
                  description={object.description}
                  members={object.members}
                  key={object.id}
                  onChange={groupLineChanged}
                  onDelete={groupLineDeleted}
                  availableName={localData?.availableNames?.personOptions}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={groupLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">External Audiences</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Information</div>
                  <div className="col-4 column-list">Contact</div>
                </div>
              </div>
              {externalList?.map((object, i) => (
                <ExternalLine
                  id={object.id}
                  name={object.name}
                  information={object.information}
                  contact={object.contact}
                  key={object.id}
                  onChange={externalLineChanged}
                  onDelete={externalLineDeleted}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={externalLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                class="btn btn-custom btn-secondary mr-3"
                onClick={(e) => {
                  e.preventDefault();
                  // history.push("/audiences");
                  // setRoleList(dummyRole);
                  // setGroupList(dummyGroup);
                  // setExternalList(dummyExternal);
                  window.location.reload();
                }}
              >
                Cancel
              </button>
              <button
                class="btn btn-custom btn-primary-custom"
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  setWasvalidated(true);
                }}
              >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
